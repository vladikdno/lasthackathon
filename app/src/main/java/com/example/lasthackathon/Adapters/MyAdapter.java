package com.example.lasthackathon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lasthackathon.Model.WeatherModel;
import com.example.lasthackathon.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onClick(WeatherModel contact);
    }

    private Context context;
    private List<WeatherModel> list;
    private OnItemClickListener listener;

    public MyAdapter(Context context, List<WeatherModel> list) {
        this.context = context;
        this.list = list;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View rowView = layoutInflater.inflate(R.layout.weatheritem, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(rowView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        String temperatureAir = list.get(position).getTemp();
        String description = list.get(position).getWeatherDescription();
        String pressureInPercent = list.get(position).getPressurePercent();
        String dateAndTime = list.get(position).getDt_txt();
        String cityName = list.get(position).getCity();

        myViewHolder.temperatureAir.setText(temperatureAir);
        myViewHolder.description.setText(description);
        myViewHolder.pressureInPercent.setText(pressureInPercent);
        myViewHolder.dateAndTime.setText(dateAndTime);
        myViewHolder.cityName.setText(cityName);

        myViewHolder.imageView.setImageResource(list.get(position).getWeatherPicture());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView temperatureAir;
        TextView description;
        TextView pressureInPercent;
        TextView dateAndTime;
        TextView cityName;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            temperatureAir = itemView.findViewById(R.id.temperatureAir);
            description = itemView.findViewById(R.id.description);
            pressureInPercent = itemView.findViewById(R.id.pressureInPercent);
            dateAndTime = itemView.findViewById(R.id.dateAndTime);
            cityName = itemView.findViewById(R.id.cityName);
            imageView = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }

    }
}
