package com.example.lasthackathon.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.lasthackathon.Model.WeatherModel;

@Database(entities = {WeatherModel.class}, version = 1) /* Это наша база данных, хранящая модельки WeatherModel и версии 1*/
public abstract class MyDataBase extends RoomDatabase {

    public abstract MyDataBaseDao getFilmsDao();

}
