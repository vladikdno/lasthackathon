package com.example.lasthackathon.database;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.lasthackathon.Model.WeatherModel;

import java.util.List;

import io.reactivex.Flowable;


@Dao
public abstract class MyDataBaseDao {

    @Insert
    public abstract void insertAll(List<WeatherModel> weatherModelsModels);

    @Query("SELECT * FROM WeatherModel")
    public abstract Flowable<List<WeatherModel>> selectAll();


    @Query("SELECT COUNT(*) FROM WeatherModel")
    public abstract Integer countAll(); /* Возвращает кол-во элементов в БД */

    @Query("DELETE FROM WeatherModel")
    public abstract void removeAll();

}