package com.example.lasthackathon.Model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class WeatherModel {

    @PrimaryKey
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private int weatherPicture;
    private String city;
    private String temp;
    private String dt_txt;
    private String weatherDescription;
    private String pressurePercent;


    public WeatherModel() {
    }

    public int getWeatherPicture() {
        return weatherPicture;
    }

    public void setWeatherPicture(int weatherPicture) {
        this.weatherPicture = weatherPicture;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getPressurePercent() {
        return pressurePercent;
    }

    public void setPressurePercent(String pressurePercent) {
        this.pressurePercent = pressurePercent;
    }

    @Ignore

    public WeatherModel(long id, int weatherPicture, String city, String temp, String dt_txt, String weatherDescription, String pressurePercent) {
        this.id = id;
        this.weatherPicture = weatherPicture;
        this.city = city;
        this.temp = temp;
        this.dt_txt = dt_txt;
        this.weatherDescription = weatherDescription;
        this.pressurePercent = pressurePercent;
    }
}
