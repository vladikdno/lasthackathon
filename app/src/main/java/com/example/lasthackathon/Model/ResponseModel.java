package com.example.lasthackathon.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel {

        @SerializedName("cod")
        @Expose
        private String cod;
        @SerializedName("message")
        @Expose
        private Double message;
        @SerializedName("cnt")
        @Expose
        private Integer cnt;
        @SerializedName("list")
        @Expose
        private java.util.List<com.example.lasthackathon.Model.ResponseModel.List> list = null;

    public java.util.List<List> getList() {
        return list;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public class List {
        @SerializedName("weather")
        @Expose
        private java.util.List<Weather> weather = null;
        @SerializedName("wind")
        @Expose
        private Wind wind;
        @SerializedName("dt")
        @Expose
        private Integer dt;
        @SerializedName("main")
        @Expose

//        private java.util.List<com.example.lasthackathon.Model.ResponseModel.Main> main = null;
//
//        public java.util.List<Main> getMain() {
//            return main;
//        }
        private Main main;
        @SerializedName("dt_txt")
        @Expose
        private String dtTxt;

        public String getDtTxt() {
            return dtTxt;
        }

        public Main getMain() {
            return main;
        }

        public Integer getDt() {
            return dt;
        }

        public java.util.List<Weather> getWeather() {
            return weather;
        }

        public Wind getWind() {
            return wind;
        }
    }
    public class Main {

        @SerializedName("temp")
        @Expose
        private Double temp;
        @SerializedName("temp_min")
        @Expose
        private Double tempMin;
        @SerializedName("temp_max")
        @Expose
        private Double tempMax;
        @SerializedName("pressure")
        @Expose
        private Double pressure;
        @SerializedName("humidity")
        @Expose
        private Integer humidity;

        public Double getTemp() {
            return temp;
        }

        public Double getPressure() {
            return pressure;
        }

        public Double getTempMin() {
            return tempMin;
        }

        public Double getTempMax() {
            return tempMax;
        }
    }
    public class Weather {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("main")
        @Expose
        private String main;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("icon")
        @Expose
        private String icon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

    }
    public class Wind {

        @SerializedName("speed")
        @Expose
        private Double speed;
        @SerializedName("deg")
        @Expose
        private Double deg;

        public Double getSpeed() {
            return speed;
        }

        public void setSpeed(Double speed) {
            this.speed = speed;
        }

        public Double getDeg() {
            return deg;
        }

        public void setDeg(Double deg) {
            this.deg = deg;
        }

    }
}




