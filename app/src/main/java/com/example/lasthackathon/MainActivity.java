package com.example.lasthackathon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.lasthackathon.Adapters.MyAdapter;
import com.example.lasthackathon.Model.ResponseModel;
import com.example.lasthackathon.Model.WeatherModel;
import com.example.lasthackathon.database.MyDataBase;
import com.example.lasthackathon.utils.ApiService;
import com.example.lasthackathon.utils.Converter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.RecyclerView)
    RecyclerView weatherView;

    static MyAdapter adapter;

    Disposable disposable;

    MyDataBase dataBase;

    CompositeDisposable disposables = new CompositeDisposable();

    List<WeatherModel> weatherModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setTitle("Weather");

        apiRequest();

        printCountries();


    }
    private void apiRequest() {
        disposable = ApiService.getAll()
                .map(new Function<ResponseModel, Boolean>() {
                    @Override
                    public Boolean apply(ResponseModel responseModel) throws Exception {
                        weatherModels = Converter.convertRequest(responseModel);
                        Log.d("www", String.valueOf(weatherModels.get(0).getDt_txt()));
                        dataBase.getFilmsDao().removeAll();
                        dataBase.getFilmsDao().insertAll(weatherModels);
                        return true;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        /*do noting*/
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
    }
    private void printCountries(){
        disposables.add(dataBase.getFilmsDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<WeatherModel>>() {
                    @Override
                    public void accept(List<WeatherModel> weatherModels) throws Exception {
                        List<WeatherModel> weatherModels1 = weatherModels;
                        adapter = new MyAdapter(MainActivity.this, weatherModels1);
                        weatherView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        weatherView.setAdapter(adapter);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("eeee", "WRRRRRONG" + "");
                    }
                }));
    }
}
