package com.example.lasthackathon.utils;

import com.example.lasthackathon.Model.ResponseModel;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {

    private static final String API = "http://api.openweathermap.org";
    private static PrivateApi privateApi;
    private static final String Q = "Kharkiv";
    private static final String MODE = "json";
    private static final String ID = "89dea4795f4998f2e6a06040f1ecc578";
    private static final String DAYS = "5";

    //http://api.openweathermap.org/data/2.5/forecast?q=Kharkiv&mode=json&appid=89dea4795f4998f2e6a06040f1ecc578&cnt=5

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<ResponseModel> getAll() {
        return privateApi.getWeather(Q, MODE, ID, DAYS);
    }

    public interface PrivateApi {
        @GET("/data/2.5/forecast")
        Observable<ResponseModel> getWeather(@Query("q") String city, @Query("mode") String json, @Query("appid") String id,
                                                  @Query("cnt") String days);
    }
}
