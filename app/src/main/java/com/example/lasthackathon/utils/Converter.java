package com.example.lasthackathon.utils;

import com.example.lasthackathon.Model.ResponseModel;
import com.example.lasthackathon.Model.WeatherModel;
import com.example.lasthackathon.R;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<WeatherModel> convertRequest(ResponseModel responseModel) {
        //WeatherModel weather = new WeatherModel(responseModel.getList().get(0).getWeather().size(), responseModel.getList().get(4).getWeather().get(0).getMain(), responseModel.getCod());
        //public WeatherModel(long id, int weatherPicture, String city, String temp, String dt_txt, String weatherDescription, String pressurePercent)
        List<WeatherModel> weather = new ArrayList<>();

        for (int i = 0; i < responseModel.getList().size(); i++) {
            ResponseModel.List result = responseModel.getList().get(i);

            String temp = String.format("%.0f С",result.getMain().getTemp() - 273.15);
            weather.add(new WeatherModel(i + 1,
                    getWeatherImage(result),
                    "Kharkiv",
                    temp,
                    result.getDtTxt(),
                    result.getWeather().get(0).getDescription(),
                    "Pressure: " + result.getMain().getPressure().toString()));
            }
            return weather;
        }

    private static int getWeatherImage(ResponseModel.List result) {
        ResponseModel.Weather image = result.getWeather().get(0);

        int imagePath;

        switch (image.getMain()) {
            case "Rain":
                imagePath = R.drawable.mightrain;
                break;
            case "Cloudy":
                imagePath = R.drawable.cloudy2;
                break;
            default:
                imagePath = R.drawable.sun1;
                break;
        }
        return imagePath;
    }


//    public static List<WeatherModel> convertRequest(ResponseModel responseModel) {
//
//        List<WeatherModel> weather = new ArrayList<>();
//
//        List<ResponseModel.Main> results = responseModel.getMains();
//
//        for (int i = 0; i < results.size(); i++) {
//
//            ResponseModel.Main weatherModel = results.get(i);
//            weather.add(new WeatherModel(i, R.drawable.ic_launcher_background,
//                    "Kharkiv",
//                    weatherModel.getTemp().toString(),
//                    weatherModel.getHumidity().toString(),
//                    weatherModel.getTempMin().toString(),
//                    weatherModel.getPressure().toString()
//            ));
//
//        }
//        return weather;
//
//    }

    }
